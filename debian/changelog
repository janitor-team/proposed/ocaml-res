ocaml-res (5.0.1-1) unstable; urgency=medium

  * Team upload
  * New upstream release (Closes: #944366)
  * Update Homepage and debian/watch
  * Remove Lifeng from Uploaders (Closes: #925073)
  * Update Vcs-*
  * Bump debhelper compat level to 12
  * Bump Standards-Version to 4.5.0
  * Add Rules-Requires-Root: no

 -- Stéphane Glondu <glondu@debian.org>  Sun, 02 Feb 2020 15:40:23 +0100

ocaml-res (4.0.3-4) unstable; urgency=medium

  * Team upload
  * Add ocamlbuild to Build-Depends

 -- Stéphane Glondu <glondu@debian.org>  Fri, 14 Jul 2017 17:26:19 +0200

ocaml-res (4.0.3-3) unstable; urgency=low

  * Team upload
  * Upload to unstable

 -- Stéphane Glondu <glondu@debian.org>  Tue, 03 Dec 2013 08:22:59 +0100

ocaml-res (4.0.3-2) experimental; urgency=low

  * Team upload
  * Build-Depend on experimental ocaml-findlib so that ocamlopt works
    properly on new native architectures

 -- Stéphane Glondu <glondu@debian.org>  Thu, 25 Jul 2013 07:28:51 +0200

ocaml-res (4.0.3-1) experimental; urgency=low

  * Team upload
  * New upstream release
  * Compile with OCaml >= 4

 -- Stéphane Glondu <glondu@debian.org>  Wed, 24 Jul 2013 21:44:13 +0200

ocaml-res (4.0.2-1) unstable; urgency=low

  [ Stefano Zacchiroli ]
  * remove myself from Uploaders

  [ Lifeng Sun ]
  * New upstream release.
  * Switch to debhelper and bump debhelper compat.
  * debian/control:
    - canonical VCS-* fields.
    - add myself to Uploaders.
    - update upstream homepage.
    - bump Standards-Version to 3.9.4.
  * debian/copyright: switch to dep5 format.
  * Fix watch file.
  * Remove patch 0001-Fix-build-on-non-native-archs.patch: fixed by the
    new upstream build system.

  [ Stéphane Glondu ]
  * Remove myself from Uploaders

 -- Lifeng Sun <lifongsun@gmail.com>  Mon, 10 Jun 2013 15:37:49 +0800

ocaml-res (3.2.0-2) unstable; urgency=low

  * Switch packaging to dh-ocaml 0.9
  * Switch packaging to source format 3.0 (quilt)
  * debian/control:
    - update my e-mail address and remove DMUA
    - update Standards-Version to 3.8.3 (no changes)

 -- Stéphane Glondu <glondu@debian.org>  Sat, 31 Oct 2009 01:58:31 +0100

ocaml-res (3.2.0-1) unstable; urgency=low

  * New Upstream Version
  * Use quilt to handle patches; add README.source
  * Update Standards-Version to 3.8.2
  * Move to section ocaml
  * Add versioned dependency to ocaml-findlib to ease OCaml 3.11.1
    transition

 -- Stephane Glondu <steph@glondu.net>  Wed, 01 Jul 2009 15:28:00 +0200

ocaml-res (3.1.1-2) unstable; urgency=low

  * Update upstream patch to make it work on bytecode archs

 -- Stephane Glondu <steph@glondu.net>  Mon, 16 Mar 2009 08:42:21 +0100

ocaml-res (3.1.1-1) unstable; urgency=low

  [ Mehdi Dogguy ]
  * Fix watch file.

  [ Stephane Glondu ]
  * New Upstream Version
  * Add dh-ocaml to Build-Depends, use ocaml.mk as a "rules" file
  * Update Standards-Version to 3.8.1
  * Add myself to Uploaders and DMUA
  * Add gbp.conf
  * Change section to libdevel as complained by Lintian
  * Don't use GPL symlink in copyright file

 -- Stephane Glondu <steph@glondu.net>  Sun, 15 Mar 2009 13:36:42 +0100

ocaml-res (2.2.5-2) unstable; urgency=low

  * uploading to unstable
  * add debian/TODO with an item about better Makefile for examples

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 09 May 2008 13:50:08 +0200

ocaml-res (2.2.5-1) experimental; urgency=low

  * Initial release (Closes: #479238)

 -- Stefano Zacchiroli <zack@debian.org>  Sat, 03 May 2008 21:57:47 +0200
